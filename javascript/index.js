var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];


var _jointype = 1;

//var track = 'keimola';
//var track = 'germany';
//var track = 'usa';
//var track = 'france';
//var track = 'elaeintarha'; 
//var track = 'imola'; 
//var track = 'england'; 
//var track = 'suzuka';
//var track = 'pentag';




// ALL FALSE FOR LIVE
var _telemetry = false;
var _consolelog = false;
var _variablebotname = false;
var _fakerace = false; 
var _slowstart = false;
var _usahack = false; 



//AI - default
var _friction = 0.01;
var _friction_static = false; // false
var _adjust_break = 1.000;


var _break_s_multiplier = 1;

var _switchlane_inspect = 8;
var _track_max_angle = 60;
var _max_friction = 0.08;

//fullspeed corner
var _fullspeed_max_angle = 30;
var _fullspeed_min_radius = 200;

// race start values
var _adjust_break_down = 0.050;
var _friction_down = 0.001;
var _friction_up = 0.00001;
var _fakerace_start_friction = 0.041;

var _vend_a = 0.98;
var _vend_b = 0.95;

var _s_1 = 1.5;
var _s_2 = 2;

//crash
var _crash_friction_down = 0.0025; //0.01;
var _use_break_case = true; // SET TO TRUE

if(_variablebotname){
	botName = botName + ' bot' + Math.floor((Math.random()*100)+1);
}
console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

if(_jointype == 1){
	client = net.connect(serverPort, serverHost, function() {
	  return send({
		msgType: "join",
		data: {
		  name: botName,
		  key: botKey
		}
	  });
	});
}else if(_jointype == 2){


	client = net.connect(serverPort, serverHost, function() {
	  return send({
		msgType: "joinRace",
		data: {
			botId: {
				name: botName,
				key: botKey
			},
			trackName: track,
			carCount: 1
		}
	  });
	});
}else if(_jointype == 3){

	client = net.connect(serverPort, serverHost, function() {
	  return send({
		msgType: "joinRace",
		data: {
			botId: {
				name: botName,
				key: botKey
			},
		}
	  });
	});

}
else if(_jointype == 4){

	client = net.connect(serverPort, serverHost, function() {
	  return send({
		msgType: "joinRace",
		data: {
			botId: {
				name: botName,
				key: botKey
			},
			"trackName": track,			
			carCount: 4			
		}
	  });
	});

}else if(_jointype == 5){

	client = net.connect(serverPort, serverHost, function() {
	  return send({
		msgType: "createRace",
		data: {
			botId: {
				name: botName,
				key: botKey
			},
			"trackName": track,
			carCount: 4			
		}
	  });
	});

}else if(_jointype == 6){

	client = net.connect(serverPort, serverHost, function() {
	  return send({
		msgType: "createRace",
		data: {
			botId: {
				name: botName,
				key: botKey
			},
			carCount: 6			
		}
	  });
	});

}

//Init localstorage
if (typeof localStorage === "undefined" || localStorage === null) {
	var LocalStorage = require('node-localstorage').LocalStorage;
	localStorage = new LocalStorage('./scratch');
}

//localstorage example
//localStorage.setItem('myFirstKey', 'myFirstValue');
//console.log(localStorage.getItem('myFirstKey'));



function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());


//car
var _color = '';
var _carname = '';



// track
var _track_id = '';
var _track_len = 0;
var _length = new Array();
var _lanes = new Array();
var _switchlane = new Array();
var _radius = new Array();
var _angle = new Array();
var _laps = 0;
var takasuora = false;
var _only200up = false;

//TODO
//number of lanes
//cars

//race info
var _quickrace = false;
var _qualifying = false;
var _race = false; 
var _tick = 0;
var _throttle = 1.00; 
var _speed = 0;
var _last_speed = 0;
var _target_speed = 0;
var _lap = -1;
var _previous_lap = -999;
var _position = 0;
var _turbo = false;
var _use_turbo = false;
var _turbo_activated = false;
var _turbo_wait_one_tick = false;
var _turbofactor = 0;
var _turbodurationticks = 0;

var _longest_distance_to_turn = 0;
var _longest_distance_start_piece = 0;
var _longest_distance_to_turn2 = 0;
var _longest_distance_start_piece2 = 0;
var _pieces_to_turn = 0;
var _car_angle = 0;
var _target_speed_max = 10; //initval - auto max
var _distance_to_turn = 0;
var _lastcorner = false;

var _ranking = 0;
var _last_lap_time = 0;

var _this_piece_number = 0;
var _this_piece_distance = 0;
var _this_carpos_angle = 0;

var _last_piece_number = 0;
var _last_piece_distance = 0;
var _last_carpos_angle = 0;

var _startlane = 0;
var _endlane = 0;

var _car_max_angle = 0;
var _full_speed = false;
var _no_speed = false;
var _max_a = 0;

var _first_angleend_found = false;
var _next_markpoint_piece = -1;
var _friction_set_for_lap = -1;
var _friction_preset_for_lap = -1;
var _last_finishline_up_friction = 0;
var _lap_start_max_angle = 0;

var _cars_to_overtake = 0;
var _carsinlane0 = 0;
var _carsinlane1 = 0;
var _carsinlane2 = 0;
var _carsinlane3 = 0;

var _lap_start_friction = 0;
var _lap_start_max_car_angle = 0;

function initStartFriction(){
	//console.log('Init Friction for: ' + _track_id);
	

	if(_track_id == 'keimola'){
		//keimola type track	
		_friction = 0.03;
		_break_s_multiplier = 1.30;
	}else if(_track_id == 'germany'){
		//germany type track	
		_friction = 0.037;
		_break_s_multiplier = 1.3;
	}else if(_track_id == 'usa'){
		//usa type track	
		_friction = 0.025;
		_break_s_multiplier = 1.5;
	}else if(_track_id == 'france'){
		//france type track	
		_friction = 0.0205; //auto setting does not work well
		_break_s_multiplier = 1.5;
	}else if(_track_id == 'elaeintarha'){
		//elaeintarha type track	
		_friction = 0.025;
		_break_s_multiplier = 1.3;
	}else if(_track_id == 'imola'){
		//imola type track	
		_friction = 0.018;
		_break_s_multiplier = 0; //1.3; //(0.2984)
	}else if(_track_id == 'england'){
		//england type track	
		_friction = 0.033;
		_break_s_multiplier = 1.5;
	}else if(_track_id == 'suzuka'){
		//suzuka type track	
		_friction = 0.037;
		_break_s_multiplier = 1.3;
	}else{
		//default for unknow track
		_friction = 0.025;
		_break_s_multiplier = 1.3;	
	}

}

function initRace(){

	_race = true;
/*
//disabled

	_friction_static = true;
	_adjust_break = _adjust_break - _adjust_break_down;
	_friction = _friction - _friction_down;
*/
}

function initMultiQuickrace(){

	//tests
	if(_fakerace){
		var variation = Math.floor((Math.random()*5))/1000;	
		_friction = _fakerace_start_friction + variation; //0.0468

		_race = true; //EMULATE RACE		
		_friction_static = true;
		_adjust_break = _adjust_break - _adjust_break_down;
		_friction = _friction - _friction_down;
	}

}
var _adjustbreakset_fix = false;
var _skipadjust = false;
function adjustAfterFirstLap(){
	//first corner
	if(!_adjustbreakset_fix && _lap == 0 && _this_piece_number > 0){

		if(_track_id != 'imola' && _track_id != 'elaeintarha'){
			if(_base_max_friction < _friction + 0.005){
				_friction = _friction - 0.005;
				_friction_set_for_lap = _lap;
				console.log('ADJUST NEW FR BASE: ' + _friction);
				_adjustbreakset_fix = true;
				_skipadjust = true;
			}else if((_max_a < 0.20 || _base_max_friction < _friction - 0.01) && _car_max_angle < 30){
				if(_friction + 0.01 < _base_max_friction){
					_friction = _friction + 0.01;
				}else{
					_friction = _base_max_friction;			
				}

				_friction_set_for_lap = _lap;
				console.log('ADJUST NEW FR: ' + _friction);
				_adjustbreakset_fix = true;
				_skipadjust = true;			
			}else{
				// do nothing
			}
		}
		var adj_brk_mult = (_max_a-0.22)*10;
		if(adj_brk_mult > 0 && _track_id != 'imola'){
			if((adj_brk_mult*2) > 0.5){
				_break_s_multiplier = _break_s_multiplier+(0.5);
				_adjust_break = _adjust_break - _adjust_break_down;
			}else{
				_break_s_multiplier = _break_s_multiplier+(adj_brk_mult*2);			
			}

			console.log('ADJUST BREAK: ' + _break_s_multiplier);
			_adjustbreakset_fix = true;
		}
		
		if(_track_id == 'imola'){
			if(_max_a < 0.2984 && _this_piece_number > 1){
				_friction = _friction + _friction_down*2;
				_adjustbreakset_fix = true;
				console.log('ADJUST NEW FR I: ' + _friction);
			}
		}

	}

}


function testFriction(){

	var this_test_r = 0;
	var test_n_peaces = 20;
	var countpieces = _length.length;
	for(var i = 0; i < test_n_peaces; i++){
		var this_test_r = _radius[_this_piece_number+i];
		if(this_test_r + '' == 'undefined'){
			//next lap
			this_test_r = _radius[_this_piece_number+i-countpieces];
		}
		if(this_test_r + '' == 'undefined' || this_test_r == ''){
			this_test_r = 0;
		}
		var this_test_r = Math.abs(this_test_r);
		if(this_test_r != 0){
			//mutka löytyi
			_first_angleend_found = true;				
			
		}else{
			//suora				
			if(_first_angleend_found){
				_next_markpoint_piece = _this_piece_number+i;
				_first_angleend_found = false;
				i = test_n_peaces; //break loop
			}

		}		
	}

}



function preSetNewFriction(){
	//console.log('***preSetNewFriction***');
	_lap_start_friction = _friction;
	_lap_start_max_car_angle = _car_max_angle;
	if(_track_max_angle-1 > _car_max_angle && (_qualifying || _quickrace) && !_friction_static){
		if(_friction_preset_for_lap != _lap){
			var angledif = _track_max_angle - _car_max_angle;
			_lap_start_max_angle = _car_max_angle;
			if(angledif >= 1){
				if(_car_max_angle/_friction/1000 >1.6){
					//console.log('NO MORE UP');
					_last_finishline_up_friction = 0;
				}else if(_car_max_angle/_friction/1000 > 0 && _car_max_angle/_friction/1000 < 1.5){
					//big step
					var multiplier = _lap;
					if(multiplier < 1){
						multiplier = 4;
					}
					multiplier = multiplier+2;

					var newf11 = (angledif/_track_max_angle*_friction/multiplier); //3
					_friction = _friction + newf11;
					_last_finishline_up_friction = newf11;
					_friction_preset_for_lap = _lap;
					//console.log('NEW friction UP3: ' + newf11);
				}else{

					//jos radalla radius 50 mutkia niin +2
					var this_min_radius = 9999;
					for(var i=0; i < _radius.length; i++){
						if(_radius[i] != '' && _radius[i] != 0 && _radius[i] < this_min_radius){
							this_min_radius = _radius[i];
						}
					}
					if(this_min_radius < 100 && _lap < 1){
						//skip
						_friction_preset_for_lap = _lap;
					}else{
						var newf1 = (angledif/_track_max_angle*_friction/10); //10
						_friction = _friction + newf1;
						_last_finishline_up_friction = newf1;
						_friction_preset_for_lap = _lap;
						//console.log('NEW friction UP10: ' + newf1);				
					}
				}			

			}
		}
	}
	if(_friction > _max_friction){
		_friction = _max_friction;
	}
	if(_race){
		if(_car_max_angle > 59.7){
			//slow a bit
			_friction = _friction-_friction_down/100;
			//console.log('RACE DOWN: 0.00001');
		}else if(_car_max_angle > 58.7){
			// a bit faster
			_friction = _friction+_friction_up;
			//console.log('RACE UP: 0.00001');			
		}
	
	}if(_race && _lap > 1){
		_friction_static = false; //unleash hell to allow setNewFriction
	}

}

function setNewFriction(){
	//console.log('***setNewFriction***');
	adjustAfterFirstLap();	
	//lap_start_friction = _friction;
	//lap_start_max_car_angle = _car_max_angle;
	var this_lap_up_already = _friction - _lap_start_friction;
	var this_lap_angle_up_already = _car_max_angle - _lap_start_max_car_angle;
	if(this_lap_up_already > 0 && !_skipadjust){
		//up
		//console.log('*** UP ALREADY this_lap_up_already' + this_lap_up_already + ' this_lap_angle_up_already' + this_lap_angle_up_already);
		if(_lap > 0){
		
		}else{
			if(_car_max_angle < 1){
				_friction_set_for_lap = -999;
				_friction = _friction+(_friction_down); //use it for up
				//set again
				//console.log('SET AGAIN 0');
			}
			if(_car_max_angle < 30){
				_friction_set_for_lap = -999;
				//set again
				//console.log('SET AGAIN');
			}
		}

	}else if(this_lap_up_already < 0){
		//down
		if(_lap > 0){
			_skipadjust = false;
		}
	}else{
		// no change
		if(_lap > 0){
			_skipadjust = false;
		}
	}

	if(_friction_set_for_lap != _lap && _car_max_angle >= _lap_start_max_angle && !_friction_static){
		var angledif = _track_max_angle - _car_max_angle;
		if(angledif >= 1 && _car_angle > 55){
			//do nothing now
		}else if(angledif >= 1 && _car_max_angle > 1 && _car_max_angle/_friction/1000 > 0 && _car_max_angle/_friction/1000 < 1.5){
			//big step
			var multiplier2 = _lap;
			if(multiplier2 < 1){
				multiplier2 = 2;
			}
			multiplier2 = multiplier2+2;

			//jos radalla radius 50 mutkia niin +2
			var this_min_radius = 9999;
			for(var i=0; i < _radius.length; i++){
				if(_radius[i] != '' && _radius[i] != 0 && _radius[i] < this_min_radius){
					this_min_radius = _radius[i];
				}
			}
			if(this_min_radius < 100 && _lap < 1){
				//skip
				_friction_set_for_lap = _lap;
			}else{
				var newf21 = (angledif/_track_max_angle*_friction/multiplier2); //3
				_friction = _friction + newf21;
				_friction_set_for_lap = _lap;
				//console.log('NEW 2 friction UP3: ' + newf21);				
			}
			
		}else if(angledif >= 1){
			var newf2 = (angledif/_track_max_angle*_friction/20);
			_friction = _friction + newf2;
			_friction_set_for_lap = _lap;
			//console.log('NEW 2 friction UP:' + newf2);
		}else if(true){
			//race
			
			_friction = _friction - ((1-angledif)*0.0001);
			_car_max_angle = _car_max_angle - angledif;			
			_friction_set_for_lap = _lap;
			//console.log('NEW 2 friction DOWN: ' + ((1-angledif)*0.0001));			
			
			_adjust_break = _adjust_break - 0.05;
		}
	}
	if(_friction > _max_friction){
		_friction = _max_friction;
	}
}

function parsePieces(json_data){
	/*
		arr_length
		arr_switchlane
		arr_radius
		arr_angle		
	*/
	var aver_length = 0;
	var this_radius = 0;
	var this_angle = 0;
	for(var i in json_data){
		if(json_data [i] + '' != 'undefined' && json_data[i].length > 0){
			_length.push(json_data[i].length);
			_radius.push('');
			_angle.push('');			
		}else if(json_data [i] + '' != 'undefined'){
			this_radius = json_data[i].radius;
			this_angle = Math.abs(json_data[i].angle);			
			aver_length = Math.round(2*3.14159*this_radius*(this_angle/360));		
			_length.push(aver_length); //count from 2*3,14159*radius*(angle/360) etc. (average)
			_radius.push(this_radius);
			_angle.push(json_data[i].angle);
		}else{
			_length.push('');
			_radius.push('');
			_angle.push('');			
		}
		
		if(json_data [i] + '' != 'undefined' && json_data[i].switch){
			_switchlane.push(true);
		}else{
			_switchlane.push(false);
		}
		_track_len += _length[i];
	}
	
	//if no 50 or 100 angles + 0.01 _friction
	var only200up = true;
	for(var i=0; i< _radius.length;i++){
		if(_radius[i] < 200 && _radius[i] > 0 && _radius[i] + '' != 'undefined' && _radius[i] != ''){
			only200up = false;
		}
	}
	if(only200up){
		_friction = _friction+0.01;
		_adjust_break = _adjust_break+0.15;
		_only200up = true;
	}
	
}

function parseLanes(json_data){
	for(var i in json_data){
		if(json_data [i] + '' != 'undefined'){
			_lanes[json_data[i].index] = json_data[i].distanceFromCenter;
		}
	}
	//console.log('Lanes:' + _lanes);
}

var _base_max_friction = 0;

function getSpeed(){
	//count speed
	var distance = 0;
	var last_dist = 0;
	//console.log(_last_piece_number);
	if(_this_piece_number == _last_piece_number){
			distance = _this_piece_distance - _last_piece_distance; // per tick
	}else{
		//last_dist = _length[_last_piece_number] - _last_piece_distance;
		//distance = last_dist + _this_piece_distance;
		distance = _last_speed;
	}
	
	if(distance == 0){
		distance = _last_speed;
	}
	
	var this_speed = Math.abs(distance);

	// tangentti kiihtyvyys
	// a = (v2-v1)/(t2-t1)
	var this_at = (this_speed-_last_speed)/(1);
	//console.log('a1=' + this_at);
	if(_max_a < this_at){
		_max_a = this_at;
	}
	//console.log('_max_a: ' + _max_a);

	// keskeiskiihtyvyys = myy * N (voi olla sama kuin myy*g)
	// a = (v*v)/r
	if(_radius[_this_piece_number] > 0){
		var this_ak = (this_speed*this_speed)/_radius[_this_piece_number];
		//console.log('a2=' + this_ak + ' myy*g: ' + _friction*9.81);
		if(this_ak > _friction*9.81){
			//console.log('WILL CRASH  _friction ->' + this_ak/9.81);
		}else{
			//console.log('TARGET  _friction ->' + this_ak/9.81);
		}
		//console.log('car angle:' + _car_angle + ' _track_max_angle:' + _track_max_angle);
		if(_car_max_angle < 58){
			/*
			// friction without slide
			_friction = (this_ak/9.81);
			console.log('SET: ' + (this_ak/9.81));
			*/
		}
		if(_base_max_friction < (this_ak/9.81)){
			_base_max_friction = (this_ak/9.81);
		}
		//console.log('_base_max_friction: ' + _base_max_friction);
	
	}
	
	_speed = this_speed;
	_last_speed = this_speed;

	//console.log('this_speed: ' + this_speed);
	return this_speed;
}

function getTargetSpeed(){
	//count target speed
	var this_target_speed = _target_speed_max;
	var g = 9.81;
	var v_start = getSpeed();

	//set max target speed (TURBO)	
	if(v_start+1 > this_target_speed){
		this_target_speed++;
		_target_speed_max = this_target_speed;
	}

	//seuraavan mutkan tarket speed
	var this_test_r = 0;
	var pieces_to_turn = 0;
	var distance_to_turn = 0;
	var test_n_peaces = 10;
	var countpieces = _length.length;
	for(var i = 0; i < test_n_peaces; i++){

		this_test_r = _radius[_this_piece_number+i];
		if(this_test_r + '' == 'undefined'){
			//next lap
			this_test_r = _radius[_this_piece_number+i-countpieces];
		}
		
		if(this_test_r + '' == 'undefined' || this_test_r == ''){
			this_test_r = 0;
		}
		var this_test_r = Math.abs(this_test_r);
		if(this_test_r != 0){
			//console.log('this_test_r' + this_test_r);
			//mutka löytyi
			pieces_to_turn = i;
			i = test_n_peaces; //break loop
		}else{
			//suora
			if(_length[_this_piece_number+i] + '' == 'undefined'){
				//next lap
				distance_to_turn = distance_to_turn + _length[_this_piece_number+i-countpieces];
			}else{
				distance_to_turn = distance_to_turn + _length[_this_piece_number+i];		
			}
		}		
	}

	//remove how much is already moved in this peace
	if(distance_to_turn != 0){
		distance_to_turn = distance_to_turn - _this_piece_distance;
	}
	//hidastuvuus
	var a = _friction*g;
	
	// v = v0 + at => v_end = v_start + (a*t)
	// kaarteessa max nopeus on v = sqrt(myy*g*r) = sqrt(a*r)
	var v_end = 0;
	if(_radius[_this_piece_number+pieces_to_turn] + '' == 'undefined'){
		v_end = Math.sqrt(a*(_radius[_this_piece_number+pieces_to_turn-countpieces]));	
	}else{
		v_end = Math.sqrt(a*(_radius[_this_piece_number+pieces_to_turn]));
	}

	//jos suora kahden mutkan välissä v_end seuraavan mukaan
	if(_radius[_this_piece_number-1] > 0 && (_radius[_this_piece_number] == 0 || _radius[_this_piece_number] == '') && _radius[_this_piece_number+1] > 0 && _radius[_this_piece_number+1] < 100){
		v_end = Math.sqrt(a*(_radius[_this_piece_number+1]));
		//console.log('Välisuora');
	}
	
	//jos kurvin jälkeen edelleen tiukkoja paloja, niin loppunopeus pienempi
	if(_radius[_this_piece_number+pieces_to_turn] == _radius[_this_piece_number+pieces_to_turn+1] && _radius[_this_piece_number+pieces_to_turn] < 100 && _radius[_this_piece_number+pieces_to_turn] > 0){
		//console.log('VEND A');
		v_end = v_end * _vend_a;
	}else if(_radius[_this_piece_number+pieces_to_turn] > _radius[_this_piece_number+pieces_to_turn+1] && _radius[_this_piece_number+pieces_to_turn] < 100 && _radius[_this_piece_number+pieces_to_turn] > 0){
		//tiukka
		//console.log('VEND B');
		v_end = v_end * _vend_b;	
	}
	

	//time to
	var t = ((v_start-v_end)/(a))+v_start; // add last tick time
		
	if(v_start < v_end){
		t = 0;
	}
	//console.log('*T:' + t);	
	//(vaadittu jarrutusmatka) distance to s=v_aver*t - added extra matka based on current speed to get it in previous tick
	//var s = ((v_start-v_end)/2)*t*v_start;
	var s = ((v_start-v_end)/2)*(t);
	if(_break_s_multiplier > 0){
		s = ((v_start-v_end)/2)*(t)*_break_s_multiplier*v_start;
		//console.log('S:' + s);
	}
	//console.log('*S:' + s);	
	_distance_to_turn = distance_to_turn;
	//console.log('*_distance_to_turn:' + _distance_to_turn);	


	//jos kurvin jälkeen edelleen tiukkoja paloja, niin loppunopeus pienempi
	if(_radius[_this_piece_number+pieces_to_turn] == _radius[_this_piece_number+pieces_to_turn+1] && _radius[_this_piece_number+pieces_to_turn] < 100 && _radius[_this_piece_number+pieces_to_turn] > 0){
		//console.log('VEND 0.98 A');
		//v_end = v_end *0.98;
		
		//console.log('S 1');
		s = s*_s_1;
	}else if(_radius[_this_piece_number+pieces_to_turn] > _radius[_this_piece_number+pieces_to_turn+1] && _radius[_this_piece_number+pieces_to_turn] < 100 && _radius[_this_piece_number+pieces_to_turn] > 0){
		//tiukka
		//console.log('VEND 0.95');
		//v_end = v_end *0.95;	

		//console.log('S 2');
		s = s*_s_2;
	}



	//full throttle on tight corner ends
	if(_radius[_this_piece_number] > 0 && _radius[_this_piece_number] <= 50 && _radius[_this_piece_number+1] == 0){
		var thisdiff = _track_max_angle - _car_angle;
		var thisdist = 0;
		//console.log(thisdiff);
		if(thisdiff >= 5){
			_no_speed = false;
			if(thisdiff < 10){
				thisdist = thisdiff/12;
			}else if(thisdiff < 30){
				thisdist = thisdiff/20;
			}
			if(_this_piece_distance/_length[_this_piece_number] > thisdist){
				//MAYBE DISABLED FOR FINAL RACE
				//console.log('FULL SPEED - Corner end');
				_full_speed = true;		
			}
		}else{
			//console.log('NO SPEED - Corner end');
			_no_speed = true;
		}
	}
	

	//console.log('_distance_to_turn' + _distance_to_turn + ' rad:' + _radius[_this_piece_number+pieces_to_turn]);
	//console.log('Multiplier: ' + s_multiplier);

	//console.log('pieces_to_turn:' + pieces_to_turn + ' distance_to_turn:' + distance_to_turn + ' v_end:' + v_end + ' t:' + t);
	//console.log(' > pieces_to_turn:' + pieces_to_turn + ' distance_to_turn:' + distance_to_turn);


	/*
			console.log(' - nopeus nyt: ' + v_start);	
			console.log(' - vaadittu loppunopeus: ' + v_end);
			//console.log(' - vaadittu aika loppunopeuden saavuttamiseen: ' + t);		
			console.log(' - vaadittu jarrutusmatka: ' + s);
	*/
	if(pieces_to_turn == 0){
		//console.log('kaarteessa');
		this_target_speed = v_end;
		
		//jos edessä vielä tiukempi mutka
		if(_radius[_this_piece_number] > _radius[_this_piece_number+1]){
			//console.log('Edessä tiukempi mutka');
			if(_radius[_this_piece_number+1] + '' != 'undefined' && _radius[_this_piece_number+1] + '' != ''){
				this_target_speed = Math.sqrt(_friction*g*((_radius[_this_piece_number+1])));
				//console.log('SEURAAVA: ' + this_target_speed + ' R:' + _radius[_this_piece_number+1] + ' P:' + _this_piece_number);
			}
		}
		
	}else if(s > distance_to_turn){
		this_target_speed = v_end;
		//console.log('jarrutetaan mutkaan: ' + distance_to_turn + ' T:' + this_target_speed);		
	}else{
		//console.log('max speed: s:' + s + ' ja distance_to_turn:' + distance_to_turn);
		this_target_speed = _target_speed_max;	
	}

	//console.log('speed/target: ' + (v_start/this_target_speed) + ' TS:' + this_target_speed);


	//full speed corner
	if(Math.abs(_radius[_this_piece_number]) >= _fullspeed_min_radius && _car_angle <= _fullspeed_max_angle){
		//console.log('Full speed corner');
		_full_speed = true;
	}

	//race end full speed
	var last_curve_piece = 0;
	for(var i=_radius.length;i>0;i--){
		if(Math.abs(_radius[i]) > 0){
			//kurvi löytyi
			last_curve_piece = i;
			i = 0; //break loop
		}else{
			// suora
		}
	}	
	if(_lap == _laps-1 && _this_piece_number >= last_curve_piece){
		//console.log('LOPPU TÄYSIÄ');
		_full_speed = true;	
	}
	_pieces_to_turn = pieces_to_turn;

	this_target_speed = this_target_speed; //TEST
	
	_target_speed = this_target_speed;
	//console.log('_target_speed' + _target_speed);
	
	return this_target_speed;
}


function getThrottle(){
	var this_throttle = 1.0;
		
	// get current speed
	var current_speed = _speed; //getSpeed();
	//console.log('current_speed:' + current_speed);

	// get target speed
	var current_target_speed = getTargetSpeed();
	//console.log('current_target_speed:' + current_target_speed);	
	
	//count throttle
	if(current_speed > current_target_speed){
		this_throttle = 0;
	}else if(current_speed <= current_target_speed){
		this_throttle = 1.0;	
	}

	var this_speed_dif = current_speed - current_target_speed;
	if(this_speed_dif > 0 && this_speed_dif < 0.10){
		this_throttle = 0.50;
	}else if(this_speed_dif < 0 && this_speed_dif > -0.10){
		this_throttle = 0.60;
	}else if(this_speed_dif > 0 && this_speed_dif < 0.25){
		this_throttle = 0.25;
	}else if(this_speed_dif < 0 && this_speed_dif > -0.25){
		this_throttle = 0.75;
	}else if(this_speed_dif == 0){
		this_throttle = 0.5;
	}
	//console.log(current_speed + ' - ' + current_target_speed + ' - ' + this_speed_dif);

	//usa quickrace tupla turbo
	if(_usahack && _only200up && _lap == 0 && _quickrace){
		this_throttle = 0.5;	
	}
	if(_usahack && _only200up && _lap == 1 && _quickrace){
		this_throttle = 0.9;	
	}

	if(current_speed == 0){
		//console.log('INIT to Full throttle');
		this_throttle = 1.0;
	}
	if(_full_speed){
		//console.log('Full throttle _full_speed');
		this_throttle = 1.0;
	}	
	
	var forcebreak = false;
	if(_car_angle > _track_max_angle-1){
		//console.log('FORCE BREAK (ANGLE TOO BIG)');
		forcebreak = true;
	}
	if(_radius[_this_piece_number] > 0 && current_speed > current_target_speed+2){
		forcebreak = true;
		//console.log('FORCE BREAK +2');
	}
/*	
	if(_car_angle > Math.abs(_last_carpos_angle)+4){
		console.log('FORCE BREAK SLIDE');
		forcebreak = true;
	}else if(_car_angle > Math.abs(_last_carpos_angle)+3.1){
		console.log('HALF FORCE SLIDE');
		this_throttle = this_throttle*0.5;
	}else if(_car_angle > Math.abs(_last_carpos_angle)+2.9){
		console.log('HALF FORCE SLIDE');
		this_throttle = this_throttle*0.75;
	}
*/

	//full speed corner between strait lanes
	if(_radius[_this_piece_number+_pieces_to_turn] + '' != 'undefined'){
		if(_radius[_this_piece_number] == '' && _radius[_this_piece_number+_pieces_to_turn] > 50 && _radius[_this_piece_number+_pieces_to_turn+1] == '' && _radius[_this_piece_number+_pieces_to_turn+2] == '' && _distance_to_turn > 200){
			//console.log('full speed corner between strait lanes');
			this_throttle = 1;
		}
		if(_distance_to_turn < 20 && _target_speed+2.5 < _speed){
			//console.log('throttle 0 (+2 too much speed)');
			this_throttle = 0;
		}
		if(_radius[_this_piece_number+1] == 50 && _speed > 6){
			//console.log('HALF SPEED - Next piece radius 50');
			this_throttle = this_throttle*0.5*_adjust_break;
		}
	}

	if(_use_break_case){
		//console.log('BREAK CASES BASED ON RADIUS:');	
		if(_radius[_this_piece_number] == 100){
			//console.log('RADIUS 100:');
			if(_car_angle > Math.abs(_last_carpos_angle)+4){
				//console.log('FORCE BREAK SLIDE');
				forcebreak = true;
			}else if(_car_angle > 58 && this_throttle > 0.65){
				//console.log('HALF FORCE SLIDE X 0.00');
				this_throttle = this_throttle*0*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+2.5 && _car_angle > 48 && this_throttle > 0.65){
				//console.log('HALF FORCE SLIDE X22 0.00');
				this_throttle = this_throttle*0*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+1.1 && _car_angle > 55 && this_throttle > 0.65){
				//console.log('HALF FORCE SLIDE X 0.25');
				this_throttle = this_throttle*0.25*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+3.3){
				//console.log('HALF FORCE SLIDE 0.25');
				this_throttle = this_throttle*0.25*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+3.2){
				//console.log('HALF FORCE SLIDE 0.5');
				this_throttle = this_throttle*0.5*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+3.1){
				//console.log('HALF FORCE SLIDE 0.65');
				this_throttle = this_throttle*0.65*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+3){
				//console.log('HALF FORCE SLIDE 0.8');
				this_throttle = this_throttle*0.80*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+2.9){
				//console.log('HALF FORCE SLIDE 0.95');
				this_throttle = this_throttle*0.95*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+2 && _car_angle > 48 && this_throttle > 0.65){
				//console.log('HALF FORCE SLIDE X 0.10');
				this_throttle = this_throttle*0.10*_adjust_break;
			}else if(_car_angle >= Math.abs(_last_carpos_angle)+1 && _car_angle > 55){
				//console.log('HALF FORCE SLIDE X 0');
				this_throttle = 0;
			}else if(_speed > 6.5 && _car_angle > Math.abs(_last_carpos_angle)+1.3 && _car_angle > 48 && this_throttle > 0.65 && _switchlane[_this_piece_number+1]){
				//console.log('HALF FORCE SLIDE X2 0.3');
				this_throttle = this_throttle*0.3*_adjust_break;
			}else if(_speed > 6.5 && _car_angle > Math.abs(_last_carpos_angle)+1.3 && _car_angle > 48 && this_throttle > 0.65 && _switchlane[_this_piece_number]){
				//console.log('HALF FORCE SLIDE X2 0.25');
				this_throttle = this_throttle*0.25*_adjust_break;
			}else if(_speed > 6.5 && _car_angle > Math.abs(_last_carpos_angle)+1.3 && _car_angle > 48 && this_throttle > 0.65 && _radius[_this_piece_number] < 100){
				//console.log('HALF FORCE SLIDE X2 0.25');
				this_throttle = this_throttle*0.25*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+0.5 && _car_angle > 48 && this_throttle > 0.65){
				//console.log('HALF FORCE SLIDE X 0.70');
				this_throttle = this_throttle*0.70*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+0.5 && _car_angle > 30 && this_throttle > 0.65 && _radius[_this_piece_number] == _radius[_this_piece_number+1] && _speed > _target_speed+0.5){
				//console.log('HALF FORCE SLIDE XX 0');
				this_throttle = 0;
			}	
		}else if(_radius[_this_piece_number] == 200){
			//console.log('RADIUS 200:');
			if(_car_angle > Math.abs(_last_carpos_angle)+4){
				//console.log('FORCE BREAK SLIDE');
				forcebreak = true;
			}else if(_car_angle > 58 && this_throttle > 0.65){
				//console.log('HALF FORCE SLIDE X 0.00');
				this_throttle = this_throttle*0*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+2.5 && _car_angle > 48 && this_throttle > 0.65){
				//console.log('HALF FORCE SLIDE X22 0.00');
				this_throttle = this_throttle*0*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+1.1 && _car_angle > 55 && this_throttle > 0.65){
				//console.log('HALF FORCE SLIDE X 0.25');
				this_throttle = this_throttle*0.25*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+3.3){
				//console.log('HALF FORCE SLIDE 0.5');
				this_throttle = this_throttle*0.5*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+3.2){
				//console.log('HALF FORCE SLIDE 0.6');
				this_throttle = this_throttle*0.6*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+3.1){
				//console.log('HALF FORCE SLIDE 0.7');
				this_throttle = this_throttle*0.7*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+3){
				//console.log('HALF FORCE SLIDE 0.9');
				this_throttle = this_throttle*0.90*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+2.5 && _car_angle > 48 && this_throttle > 0.5){
				//console.log('HALF FORCE SLIDE X 0.10');
				this_throttle = this_throttle*0.10*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+0.5 && _car_angle > 48 && this_throttle > 0.65){
				//console.log('HALF FORCE SLIDE X 0.50');
				this_throttle = this_throttle*0.50*_adjust_break;
			}else if(_speed > _target_speed+1 && _radius[_this_piece_number] > _radius[_this_piece_number+1]){
				//console.log('HALF FORCE SLIDE XX 0');
				this_throttle = this_throttle*0*_adjust_break;
			}
		
		}else if(_radius[_this_piece_number] == 50){
			//default case //50
			//console.log('RADIUS 50:');
			if(_car_angle > Math.abs(_last_carpos_angle)+4){
				//console.log('FORCE BREAK SLIDE');
				forcebreak = true;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+2.5 && _radius[_this_piece_number] < 100  && _radius[_this_piece_number+1] < 100){
				//console.log('HALF FORCE SLIDE Z1 0');
				this_throttle = 0;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+2.5 && _radius[_this_piece_number] < 100){
				//console.log('HALF FORCE SLIDE Z2 0.75');
				this_throttle = this_throttle*0.75*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+3.3){
				//console.log('HALF FORCE SLIDE 0.25');
				this_throttle = this_throttle*0.25*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+3.2){
				//console.log('HALF FORCE SLIDE 0.5');
				this_throttle = this_throttle*0.5*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+3.1){
				//console.log('HALF FORCE SLIDE 0.65');
				this_throttle = this_throttle*0.65*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+3){
				//console.log('HALF FORCE SLIDE 0.8');
				this_throttle = this_throttle*0.80*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+2.9){
				//console.log('HALF FORCE SLIDE 0.95');
				this_throttle = this_throttle*0.95*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+2 && _car_angle > 55 && this_throttle > 0.65){
				//console.log('HALF FORCE SLIDE X 0.0');
				this_throttle = 0;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+2 && _car_angle > 48 && this_throttle > 0.65){
				//console.log('HALF FORCE SLIDE X 0.10');
				this_throttle = this_throttle*0.10*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+1 && _car_angle > 55){
				//console.log('HALF FORCE SLIDE X 0');
				this_throttle = 0;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+0.5 && _car_angle > 55 && this_throttle > 0.65 && _radius[_this_piece_number] < 100){
				//console.log('HALF FORCE SLIDE X 0.35');
				this_throttle = this_throttle*0.35*_adjust_break;
			}else if(_speed > 6.5 && _car_angle > Math.abs(_last_carpos_angle)+1.3 && _car_angle > 48 && this_throttle > 0.65 && _radius[_this_piece_number] < 100){
				//console.log('HALF FORCE SLIDE X2 0.25');
				this_throttle = this_throttle*0.25*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+0.5 && _car_angle > 48 && this_throttle > 0.65 && _radius[_this_piece_number+1] < 100){
				//console.log('HALF FORCE SLIDE X 0.35');
				this_throttle = this_throttle*0.35*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+0.5 && _car_angle > 48 && this_throttle > 0.65){
				//console.log('HALF FORCE SLIDE X 0.70');
				this_throttle = this_throttle*0.70*_adjust_break;
			}else if(_car_angle > Math.abs(_last_carpos_angle)+1.5 && _car_angle > 48 && this_throttle > 0.5){
				//console.log('HALF FORCE SLIDE XZ 0.50');
				this_throttle = this_throttle*0.50*_adjust_break;
			}
		}
	}
	
	//console.log(_lastcorner);
	if(_lastcorner && _car_angle > 55 && _radius[_this_piece_number] == 200){
		//console.log('LAST CORNER - ANGLE 55 RAD 200 -> HALF SPEED');
		this_throttle = 0.5*this_throttle;
	}

	if(_lastcorner && _car_angle > 58){
		//console.log('LAST CORNER - ANGLE 58 -> NO SPEED');	
		this_throttle = 0*this_throttle;
	}else if(_lastcorner && _car_angle > 55){
		//console.log('LAST CORNER - ANGLE 55 -> HALF SPEED');	
		this_throttle = 0.5*this_throttle;
	}

	if(_distance_to_turn < 300 && _speed > 10){
		//console.log('FORCE BREAK - DISTANCE < 300, SPEED > 10');	
		forcebreak = true;
	}
	
	if(_no_speed || forcebreak){
		this_throttle = 0;	
	}

	if((_quickrace && _lap != _laps-1 && takasuora)){
		//use turbo last round
		_turbo_wait_one_tick = true;
		_turbo = false;
	}

	if(_slowstart && _tick < 10){
		this_throttle = this_throttle*0.75;
	}

	//console.log('S: ' + current_speed + ' - T: ' + current_target_speed + ' - TR:' + this_throttle);

	if(this_throttle > 1){
		this_throttle = 1;
	}

	_throttle = Math.round(this_throttle*100)/100;	
	return this_throttle;
}


function getLane(){
	//left or right or keep
	// suunta voimakkuus x kulma x (säde)

	
	var switchLane = '';
	var sum = 0;
	var countpieces = _angle.length;
	var next_piece = false;
		
	for(var i=0; i<_switchlane_inspect; i++){
		if(_radius[_this_piece_number+i] + '' != 'undefined' && _radius[_this_piece_number+i] != 0){
			if(_angle[_this_piece_number+i] + '' != 'undefined'){
				sum = sum + _angle[_this_piece_number+i];
				//console.log(_angle[_this_piece_number+i]);
			}else{
				sum = sum + _angle[_this_piece_number+i-countpieces];			
			}
		}	
	}
	
	//console.log('next lane:' + sum);
	if(sum < 0){
		switchLane = 'Left';
	}else if(sum > 0){
		switchLane = 'Right';	
	}


	//normaalitapauksessa lähetä viesti vain jos switch pala seuraavassa	
	if(_switchlane[_this_piece_number+1] + '' == 'undefines'){
		if(_switchlane[_this_piece_number+1-countpieces] > 0){
			//send Left / Right
		}else{
			//do not send anything
			switchLane = '';			
		}	
	}else{
		if(_switchlane[_this_piece_number+1] > 0){
			//send Left / Right
		}else{
			//do not send anything
			switchLane = '';			
		}	
	}
	

	//jos vauhtia on paljon ja tiukka mutka edessä käytä ulkokaarta
	if(_radius[_this_piece_number+2] + '' == 'undefined' && _radius[_this_piece_number+2-countpieces] <= 50 && _radius[_this_piece_number+2-countpieces] > 0){
		//Tiukka mutka tulossa
		if(_switchlane[_this_piece_number+1-countpieces]){
			//on vaihtopala -> koitetaan ulkoreunaan
			if(_speed > _target_speed * 1.6){
				//tullaan kovaa
				//console.log('Ulkokaarre');
				if(_angle[_this_piece_number+2-countpieces] > 0){
					switchLane = 'Left';
				}else{
					switchLane = 'Right';			
				}
			}
		}	
	}else if(_radius[_this_piece_number+2] <= 50 && _radius[_this_piece_number+2] > 0){
		//Tiukka mutka tulossa
		//console.log('tiukka mutka tulossa');
		if(_switchlane[_this_piece_number+1]){
			//on vaihtopala -> koitetaan ulkoreunaan
				//console.log('on vaihtopala');			
			if(_speed > _target_speed * 1.5 || _target_speed == _target_speed_max){
				//tullaan kovaa
				//console.log('Ulkokaarre:' + _angle[_this_piece_number+2]);
				if(_angle[_this_piece_number+2] > 0){
					switchLane = 'Left';
				}else{
					switchLane = 'Right';			
				}
			}
		}
	
	}

	var lastcorner_piece_start = 999;
	var kf = false;
	for(var i=_radius.length; i > 0; i--){
		if(_radius[i] == '' || _radius[i] == 0){
			//suora
			if(kf){
				lastcorner_piece_start = i+1;
				i = 0; //break

			}
		}else if(_radius[i] + '' != 'undefined'){
			//kaarre
			kf = true;
		}
	}
	
	_lastcorner = false;
	if(_this_piece_number > lastcorner_piece_start && _radius[_this_piece_number] > 0){
		_lastcorner = true;
		//console.log('LASTCORNER');
	}

	if(_car_angle > 30 && !_lastcorner){
		//if swich in angle or after angle if car angle is > 30 - use outer lane
		if(_radius[_this_piece_number] > 0 && _radius[_this_piece_number+1] > 0 && _switchlane[_this_piece_number+1]){
			//use outer lane if in inner lane			
			if(_startlane > 0){
				if(_car_angle < 50){
					_full_speed = true;
				}
				//console.log('FULL SPEED - LAST CORNER SWITCHLANE');
				if(_angle[_this_piece_number+1] > 0){
					switchLane = 'Left';
				}else{
					switchLane = 'Right';			
				}			
			}
		}
	}

		
	// Simppeli ohitus syteemi
	if(_cars_to_overtake > 0){
		//console.log('ODOTTAA OHITUSKOHTAA');
	}
	
	if(_cars_to_overtake > 0 && (_this_piece_number > lastcorner_piece_start || _this_piece_number == _longest_distance_to_turn-2 || _this_piece_number == _longest_distance_to_turn-1 || _this_piece_number == _longest_distance_to_turn || lastcorner_piece_start == 0)){
		//console.log('VAIHDETAAN KAISTAA');
		if(switchLane == 'Right'){
			switchLane = 'Left';
		}else if(switchLane == 'Left'){
			switchLane = 'Right';			
		}
	}
	
	//console.log(switchLane);	
	return switchLane;
}


jsonStream.on('data', function(data) {

	var util = require('util'); // for console.log debugging


	var dd = data.data;
	var set_this_throttle = 0;
	if (data.msgType === 'carPositions') {

		for(var i = 0; i < dd.length; i++) {
			var this_dd = dd[i];
			
			if(this_dd.id.color == _color){
				_previous_lap = _lap;
				_lap = this_dd.piecePosition.lap;			
			
				var newlap = false;
				if(_lap != _previous_lap){
					newlap = true;
				}
		
				var newpiece = false;
				if(_this_piece_number != _last_piece_number){
					newpiece = true;
				}		
			

						
				_tick = data.gameTick;
				_last_piece_number = _this_piece_number;
				_last_piece_distance = _this_piece_distance;
				_last_carpos_angle = _this_carpos_angle;
				_this_carpos_angle = this_dd.angle;
				_this_piece_distance = this_dd.piecePosition.inPieceDistance;			
				_this_piece_number = this_dd.piecePosition.pieceIndex;			
				_startlane = this_dd.piecePosition.lane.startLaneIndex;
				_endlane = this_dd.piecePosition.lane.endLaneIndex;
			
				_car_angle = Math.abs(this_dd.angle);
				if(_car_max_angle < _car_angle){
					_car_max_angle = _car_angle;
				}


				if(newpiece){
					testFriction();
					if(_next_markpoint_piece-1 == _this_piece_number){
						//console.log('MARKER PIECE: ' + _next_markpoint_piece);
						setNewFriction();
					}
					//NEW
					/*
					if(_car_max_angle < 10){
						_friction = _friction + 0.01;
					}
					*/
				}
				if(newlap){
					preSetNewFriction();
					//_friction = _friction*0.5; // NEW
				}

				set_this_throttle = getThrottle();


				if(newpiece){

					/*				
					// ignore mid rad 200
					var this_next_r = _radius[_this_piece_number+_pieces_to_turn];
					var this_next_next_r = _radius[_this_piece_number+_pieces_to_turn+1];
					var this_next_l = _length[_this_piece_number+_pieces_to_turn];
					var this_next_next_l = _length[_this_piece_number+_pieces_to_turn+1];
					
					var full_distance_to_turn = _distance_to_turn;
					//console.log(_pieces_to_turn);
					//console.log(this_next_r);
					if(_pieces_to_turn > 1 && this_next_r == 200 && (this_next_next_r < 50 || this_next_next_r == '')){
						full_distance_to_turn = full_distance_to_turn + this_next_l + this_next_next_l;
					}
					
					if(full_distance_to_turn > _longest_distance_to_turn2){
						_longest_distance_to_turn2 = full_distance_to_turn;
						_longest_distance_start_piece2 = _this_piece_number;
					}					
					*/
					//normal
					if(_distance_to_turn > _longest_distance_to_turn){
						_longest_distance_to_turn = _distance_to_turn;
						_longest_distance_start_piece = _this_piece_number;
					}
					/*
					if(_longest_distance_to_turn2 > _longest_distance_to_turn && _this_piece_number > 5 && _this_piece_number < _length.length-1){
						//console.log('USE 2 TURBO');
						_longest_distance_to_turn = _longest_distance_to_turn2;
						_longest_distance_start_piece = _this_piece_number;
					}
					*/
					
				}
				
				//turbo saving for later use				
				if((_usahack && _only200up && _lap == 2 && _quickrace)){
					//handle usa tupla turbo
					if( _this_piece_number == _longest_distance_start_piece-1 && _car_angle < 40) {
						_use_turbo = true;
					}else if(_distance_to_turn > 290 || (_distance_to_turn > 200 && _this_piece_number == _longest_distance_start_piece)) {
						_use_turbo = true;
					}else if(_distance_to_turn > 200 && _laps-1 == _lap && _this_piece_number > _longest_distance_start_piece){
						//for quickrace
						_use_turbo = true;				
					}else{
						_use_turbo = false;
				
					}				
				
				}else if(_usahack && _only200up && _lap != 2 && _quickrace){
					//save turbo
				}else{
					if( _this_piece_number == _longest_distance_start_piece-1 && _car_angle < 40) {
						_use_turbo = true;
						//console.log('Set turbo 1: ' + _turbodurationticks);
					}else if(_distance_to_turn > (_turbodurationticks*10-10) && _turbodurationticks > 0 || (_distance_to_turn > 200 && _this_piece_number == _longest_distance_start_piece && _longest_distance_start_piece > 1)) {
						_use_turbo = true;
						//console.log('Set turbo 2: ' + _turbodurationticks + ' _longest_distance_start_piece:' + _longest_distance_start_piece);						
					}else if(_distance_to_turn > 200 && _laps-1 == _lap && _this_piece_number > _longest_distance_start_piece && _longest_distance_start_piece > 1){
						//for quickrace
						_use_turbo = true;
						//console.log('Set turbo 3: ' + _turbodurationticks + ' _longest_distance_start_piece:' + _longest_distance_start_piece);						
					}else{
						_use_turbo = false;
				
					}				
				}


				//console.log('_longest_distance_start_piece: ' + _longest_distance_start_piece);

				if(newpiece){
					_full_speed = false;
					_no_speed = false;
					var newlane = getLane();
										
					if(newlane == 'Left'){
						//console.log('LANE: ' + newlane);					
						send({
							msgType: "switchLane",
							data: "Left",
							"gameTick": data.gameTick
						});
				
					}else if(newlane == 'Right'){
						//console.log('LANE: ' + newlane);					
						send({
							msgType: "switchLane",
							data: "Right",
							"gameTick": data.gameTick
						});
				
					}else{
						// SEND
					
						if(_turbo && _use_turbo){

							send({
								msgType: "turbo",
								data: "FOOOO...",
								"gameTick": data.gameTick								
							});

							//console.log('**********');						
							console.log('TURBO USED');
							//console.log('**********');							
							_turbo = false;
							_turbo_activated = true;
							
							//_friction = _friction*0.5; // NEW

						}else{
							send({
								msgType: "throttle",
								data: set_this_throttle,
								"gameTick": data.gameTick
							});	
						}
					}
				}else{

					if(_turbo && _use_turbo){

						send({
							msgType: "turbo",
							data: "FOOOO...",
							"gameTick": data.gameTick								
						});
					
						//console.log('**********');						
						console.log('TURBO USED');
						//console.log('**********');	
						_turbo = false;
						_turbo_activated = true;

						//_friction = _friction*0.5; // NEW
	
					}else{
						send({
							msgType: "throttle",
							data: set_this_throttle,
							"gameTick": data.gameTick
						});	
					}

		
				}
			
				var this_tick = data.gameTick;
				if(this_tick + '' == 'undefined'){
					this_tick = 0;
				}
				if(_consolelog){
					console.log(this_tick + ' ' +  _color + ':' + _carname + ' S:' + Math.round(_speed*100)/100 + ' TS:' + Math.round(_target_speed*100)/100 + ' T:' + _throttle + ' ' + ' FR:' + Math.round(_friction*10000)/10000 + ' CA:' + Math.round(_car_angle*10)/10 + ' CAmax:' + Math.round(_car_max_angle*10000)/10000 + ' X:' + Math.round(_car_max_angle/_friction/1000*1000)/1000 + ' D:' + Math.round(_distance_to_turn) + ' T:' + _turbo);				
				}
				if(_telemetry){
					//Telemetry (could be write to limit based on ticks eg. max 300)
					if(data.gameTick + '' != 'undefined' && data.gameTick < 40000){
						var telemetry = '{"telemetry":{"tick":' + data.gameTick + ',"speed":' + _speed + ',"target_speed":' + _target_speed + ',"throttle":' + _throttle + ',"angle":' + _car_angle/10 + ',"friction":' + _friction*100 + '}}';
				
				
						localStorage.setItem('telemetry_' + data.gameTick, telemetry); // track				

						//var this_dd_str = JSON.stringify(this_dd);
						//localStorage.setItem('carPositions_' + data.gameTick, this_dd_str); // track
					}
				}

				if(_turbo_wait_one_tick){
					_turbo_wait_one_tick = false;
					_turbo = true;
				}
			
				if(newpiece){
					console.log('-- ' + _this_piece_number + ' (' + _lap + '/' + _laps + ') ' + _color + ' --' + ' RAD:' + _radius[_this_piece_number] + ' ANG:' + _angle[_this_piece_number] + ' L:' + _startlane + ' ' + newlane + ' CMAX:' + Math.round(_car_max_angle*100)/100 + ' FR:' + Math.round(_friction*100000)/100000 + ' S:' + Math.round(_speed*100)/100 + ' MAXA:' + Math.round(_max_a*10000)/10000 + ' MBF:' + Math.round(_base_max_friction*10000)/10000 + ' T:' + _throttle);
				}
			}else{
				//console.log('other car info:' + this_dd.id.name);
				
				//overtake
				/*
				var _cars_to_overtake = 0;
				var _carsinlane0 = 0;
				var _carsinlane1 = 0;
				var _carsinlane2 = 0;
				var _carsinlane3 = 0;				
				*/
				var other_pieceIndex = this_dd.piecePosition.pieceIndex;
				var other_inPieceDistance = this_dd.piecePosition.inPieceDistance;
				var other_endLaneIndex = this_dd.piecePosition.lane.endLaneIndex;
				
				//console.log('pieceIndex:' + this_dd.piecePosition.pieceIndex);
				//console.log('inPieceDistance:' + this_dd.piecePosition.inPieceDistance);
				//console.log('endLaneIndex:' + this_dd.piecePosition.lane.endLaneIndex);
				

				
				if(((_this_piece_number == other_pieceIndex && other_inPieceDistance > _this_piece_distance) || _this_piece_number+1 == other_pieceIndex) && other_endLaneIndex == _endlane){
					//suoraan edessä samalla kaistalla
					var cfdif = 0;
					if(_this_piece_number == other_pieceIndex){
						cfdif = other_inPieceDistance - _this_piece_distance;
					}else if(_this_piece_number+1 == other_pieceIndex){
						cfdif = (_length[_this_piece_number] - _this_piece_distance) + other_inPieceDistance;
					}
					
					//console.log('AUTO EDESSÄ: ' + cfdif);
					_cars_to_overtake = _cars_to_overtake+1;
					if(_endlane == 0){
						_carsinlane0 = _carsinlane0 + 1;
					}else if(_endlane == 1){
						_carsinlane1 = _carsinlane1 + 1;
					}else if(_endlane == 2){
						_carsinlane2 = _carsinlane2 + 1;
					}else if(_endlane == 3){
						_carsinlane3 = _carsinlane3 + 1;
					}
					
				}else if(( _this_piece_number+1 == other_pieceIndex) && (other_endLaneIndex == _endlane+1 || other_endLaneIndex == _endlane-1)){
					// lähellä mutta viereisessä palassa
					//console.log('AUTO VIERESSÄ EDESSÄ');
					if(_endlane == 0){
						_carsinlane0 = _carsinlane0 + 1;
					}else if(_endlane == 1){
						_carsinlane1 = _carsinlane1 + 1;
					}else if(_endlane == 2){
						_carsinlane2 = _carsinlane2 + 1;
					}else if(_endlane == 3){
						_carsinlane3 = _carsinlane3 + 1;
					}			
				}

			}		
			//overtake 2
			/*
			console.log('_cars_to_overtake:' + _cars_to_overtake);
			console.log('_endlane:' + _endlane);
			console.log('_carsinlane0:' + _carsinlane0);
			console.log('_carsinlane1:' + _carsinlane1);		
			console.log('--');
			*/
			var resetcarlanes = false;
			if(_endlane == 0 && _cars_to_overtake > 0 && _carsinlane1 == 0){
				// OK - toinen kaista vapaa
				//console.log('OK1');
			}else if(_endlane == 1 && _cars_to_overtake > 0 && _carsinlane0 == 0){
				// OK - toinen kaista vapaa
				//console.log('OK2');
			}else if(_cars_to_overtake > 0){
				// toinenkin kaista tukossa
				resetcarlanes = true;
			}else if(_cars_to_overtake == 0){
				// ei ketään näkösällä
				resetcarlanes = true;			
			}
			
			if(resetcarlanes){
				//console.log('RESET');
				_cars_to_overtake = 0;
				_carsinlane0 = 0;
				_carsinlane1 = 0;
				_carsinlane2 = 0;
				v_carsinlane3 = 0;				
			}
		}		
	} else {	
	

	
		if (data.msgType === 'join') { 
			console.log('Joined Race:' + dd.name);

		} else if (data.msgType === 'yourCar') {
			_color = dd.color;
			console.log('My car is: ' + dd.color);
			
		} else if (data.msgType === 'gameInit') {
			console.log('gameInit');
			if(dd.race.raceSession.laps + '' != 'undefined'){
				//RACE OR QUICKRACE
				_laps = dd.race.raceSession.laps;
				_quickrace = dd.race.raceSession.quickRace;
				_qualifying = false;				
			}else{
				//Qualifying
				_qualifying = true;
				_laps = 999;
				_quickrace = false;
			}
			for(var i=0; i < dd.race.cars.length; i++){		
				if(dd.race.cars[i].id.color == _color){
					_carname = dd.race.cars[i].id.name;
				}
				console.log('CAR ' + dd.race.cars[i].id.color + ': ' + dd.race.cars[i].id.name);
			}

			_track_id = dd.race.track.id;
			
			//number of lanes
			console.log('Lanes: ' + dd.race.track.lanes.length);
			
			//if(!(_length.length > 0)){
				parsePieces(dd.race.track.pieces);
			//}
			//if(!(_lanes.length > 0)){
				parseLanes(dd.race.track.lanes);
			//}
			
			//console.log('Results:' + util.inspect(_radius, false, null));
			if(_quickrace){
				initMultiQuickrace();
				console.log('QUICK Race started: ' + dd.race.track.id);
				console.log('Lap:' + _lap);
				console.log('Laps:' + _laps);
				if(_telemetry){
					localStorage.setItem('init_' + dd.race.track.id, JSON.stringify(dd)); // track	
				}
				initStartFriction();				
			}else if(_qualifying){
				console.log('Qualifying started:' + dd.race.track.id);
				console.log('Lap:' + _lap);
				console.log('Laps:' + _laps);
				initStartFriction();
			}else{
				initRace();
				console.log('Race started:' + dd.race.track.id);
				console.log('Lap:' + _lap);
				console.log('Laps:' + _laps);
			
			}
			
		} else if (data.msgType === 'gameStart') {
			console.log('gameStart');
		
		} else if (data.msgType === 'crash') {

			console.log('**********');						
			console.log('Crash:' + dd.color);
			console.log('**********');		

			if(_color == dd.color){
				_max_friction = _friction - _friction_down;
				_friction = _friction - _crash_friction_down;
				_car_max_angle = 0;
				if(_track_id == 'imola'){
					_break_s_multiplier = 1.3;
				}
			}
			
			_throttle = 0; 
			_speed = 0;
			_last_speed = 0;
			_target_speed = 0;			
			
		} else if (data.msgType === 'spawn') {
			console.log('Back on track:' + dd.color);
			if(_color == dd.color){
				_throttle = 1.00; 
			}
		} else if (data.msgType === 'lapFinished') {
			console.log('lapFinished:' + dd.car.color + ' Ranking:' + dd.ranking.overall);
			if(_color == dd.car.color){
				_ranking = dd.ranking.overall;
				console.log('LAP DIF TICKS: ' + (dd.lapTime.ticks - _last_lap_time));
				_last_lap_time = dd.lapTime.ticks;
			}
			
		} else if (data.msgType === 'dnf') {
			console.log('disqualified/disconnected:' + dd.car.color);
			
		} else if (data.msgType === 'finish') {
			console.log('Finish:' + dd.color);
			
		} else if (data.msgType === 'gameEnd') {
			console.log('Race ended');
			console.log('Results:' + util.inspect(dd.results, false, null));
			
			
			//reset for race
			_tick = 0;
			_throttle = 1.00; 
			_speed = 0;
			_last_speed = 0;
			_target_speed = 0;
			_lap = -1;
			_previous_lap = -999;
			_position = 0;
			_turbo = false;

			_car_angle = 0;
			_target_speed_max = 10; //initval - auto max
			_distance_to_turn = 0;

			_this_piece_number = 0;
			_this_piece_distance = 0;
			_this_carpos_angle = 0;

			_last_piece_number = 0;
			_last_piece_distance = 0;
			_last_carpos_angle = 0;

			_startlane = 0;
			_endlane = 0;

			//_car_max_angle = 0; //keep old
			_full_speed = false;
			_no_speed = false;

			_first_angleend_found = false;
			_next_markpoint_piece = -1;
			_friction_set_for_lap = -1;
			_friction_preset_for_lap = -1;	
			
			_friction = _friction - _last_finishline_up_friction; //fix last up
			
		} else if (data.msgType === 'tournamentEnd') {
			console.log('Tournament ended');
		} else if (data.msgType === 'turboAvailable') {
				
			console.log('TURBO AVAILABLE X' + dd.turboFactor + ' TIME: ' + dd.turboDurationTicks);
			
			_turbofactor = dd.turboFactor;
			_turbodurationticks = dd.turboDurationTicks;
			
			_turbo_wait_one_tick = true;
		} else if (data.msgType === 'turboEnd') {
			if(_color == dd.color){
				console.log('TURBO END');
				_turbo_activated = false;			
			}
		} else{
			console.log('Other message:' + data.msgType);
			console.log('Other message:' + util.inspect(dd, false, null));
		}
		send({
			msgType: "ping",
			data: {}
		});
	}
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});